#!/bin/bash

if [ ! -x $(which jq) ]; then 
	echo "ERROR: requires jq - command-line JSON processor"
	exit 1
fi

DAEMONS=$(cat config.json | jq -Mc ".daemons[]" | sed -e 's/"//g')

if [ -x /bin/systemctl ]
then 
	for i in $DAEMONS; do
		echo -en "$i \t"
		systemctl is-active $i
	done
else
	for i in $DAEMONS; do
		COUNT=$(ps --ppid 2 --pid 2 --deselect -o tty,args | grep ^? | grep -c $i)
		echo -ne "$i \t"
		if (( $COUNT > 0 )); then 
			echo active
		else
			echo inactive
		fi

	done
fi	

exit 0




