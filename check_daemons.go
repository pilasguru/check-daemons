package main

import (
    "bytes"
    "fmt"
    "os/exec"
    "os"
    "./Config"
)

func getProcessSystemd(processName string) {

   // systemctl is-active $i
   cmd := exec.Command("/bin/systemctl", "is-active", processName)
   var out bytes.Buffer
   cmd.Stdout = &out
   err := cmd.Run()
   if err != nil {
	//startProcess(processName)
   }
   fmt.Print(processName, "\t",  out.String())
   return
}

func getProcessInit(processName string) {
   return
}

func main() {
   config := Config.LoadConfig()

   for _, daemon := range config.Daemons {
      if _, err := os.Stat("/bin/systemctl"); err == nil {
         getProcessSystemd(daemon)
      }
      getProcessInit(daemon)
   }
}
