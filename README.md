# Check daemons

This script check a list of daemons and print active/inactive status of each one

Versions:

* Bash scripting
* Go

## Configuration

List of daemons:  `config.json`

## Go execution

```
go build check_daemons.sh
./check_daemons
```


